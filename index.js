let fs = require('fs'),
    ytdl = require('ytdl-core'),
    express = require('express'),
    http = require('http'),
    path = require('path'),
    app = express(),
    engine = require('consolidate'),
    getStat = require('util').promisify(fs.stat);


app.set('views',path.join(__dirname,'view'));
app.use(express.static(path.join(__dirname,'static/downloaded_mp3')));



app.engine('html', engine.mustache);
app.set('view engine', 'html');

app.get('/', (req,res) =>
{
    res.render('index');
});

var file_name = '';

app.get('/DownloadVideo', (req,res) =>
{
    var url = req.query.videoUrl;
    var dest_dir = 'static/downloaded_mp3' ;

    var video_readable_stream = ytdl(url,{ filter: 'audioonly'} );
    file_name = dest_dir + '/';


    ytdl.getInfo( url, function(err, info){

        if( err ) throw err;

        var video_name = info.title.replace( '|', '').toString('ascii');

        file_name += video_name.trim().replace(/ /g,'_') + '.mp3';

        var video_writable_stream = fs.createWriteStream( file_name );

        var stream = video_readable_stream.pipe(video_writable_stream);

         stream.on('finish', function() {
             res.writHead(200);
             res.end();
         });

    });


    res.redirect('FileGenerated');

});

app.get('/FileGenerated', (req,res) =>
{
    res.render('file_generated');
});

app.get('/Mp3File', async (req,res) =>
{
    res.render('download_file');

    const stat = await getStat(file_name);

    console.log(stat);

    res.writeHead(200, {
        'Content-Type': 'audio/mpeg3',
        'Content-Length': stat.size, 
        'Content-Disposition': 'attachment; filename='+file_name
     });

    res.download(file_name);
});



http.createServer(app).listen(process.env.PORT || 3000);

